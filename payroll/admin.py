from django.contrib import admin
from .models import *
from import_export.admin import ImportExportModelAdmin


# Register your models here.
@admin.register(Employees, Salary, Position, Attendance, Payment, Deduction, Overtime, Admin)
class ViewAdmin(ImportExportModelAdmin):
    pass


admin.site.register(Profile)