from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.contrib.auth.models import User
from django.db import models


# Create your models here.

def validate_number(value):
    if value:  # Your conditions here
        raise ValidationError('%s some error message' % value)


class Employees(models.Model):
    row_id = models.IntegerField(blank=True)
    emp_id = models.IntegerField(primary_key=True, validators=[MinValueValidator(1)])
    scd1_id = models.IntegerField()
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.TextField()
    DOB = models.DateField()
    contact_info = models.CharField(max_length=50)

    choices = (
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('Unspecified', 'Others')
    )

    gender = models.CharField(choices=choices,max_length=50)
    post_id = models.IntegerField()
    post_scd_id = models.IntegerField()
    current_flag = models.CharField(max_length=50)
    effective_from = models.DateField()
    effective_to = models.DateField()
    created_on = models.DateField()
    updated_on = models.DateField()

    def __str__(self):  # used by django admin to represent the certain object
        return 'Employee : {0}'.format(self.emp_id)


class Salary(models.Model):
    row_id = models.IntegerField(primary_key=True, validators=[MinValueValidator(1)])
    emp_id = models.ForeignKey(Employees, on_delete=models.CASCADE) #oneToMany
    emp_scd1_id = models.IntegerField()
    basic_scale = models.FloatField()
    other_sal1 = models.FloatField()
    other_sal2 = models.FloatField()
    other_sal3 = models.FloatField()
    net_salary = models.FloatField()
    current_flag = models.CharField(max_length=50)
    effective_from = models.DateField()
    effective_to = models.DateField()
    created_on = models.DateField()
    updated_on = models.DateField()

    def __str__(self):  # used by django admin to represent the certain object
        return 'Salary : {0}'.format(self.emp_id)


class Position(models.Model):
    row_id = models.IntegerField(primary_key=True)
    scd1_id = models.IntegerField()
    description = models.CharField(max_length=50)
    current_flag = models.CharField(max_length=50)
    effective_from = models.DateField()
    effective_to = models.DateField()
    created_on = models.DateField()
    updated_on = models.DateField()

    def __str__(self):  # used by django admin to represent the certain object
        return 'Position : {0}'.format(self.emp_id)


class Attendance(models.Model):
    row_id = models.IntegerField(primary_key=True)
    emp_id = models.ForeignKey(Employees, on_delete=models.CASCADE) #oneToMany
    date = models.DateField()
    checkin_time = models.TimeField()
    status = models.CharField(max_length=50)
    checkout_time = models.TimeField()
    hours = models.FloatField() #automatic

    def __str__(self):  # used by django admin to represent the certain object
        return 'Attendance : {0}'.format(self.emp_id)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    def __str__(self):
        return f'{self.user.username} Profile'

class Payment(models.Model):
    row_id = models.IntegerField(primary_key=True)
    emp_id = models.ForeignKey(Employees, on_delete=models.CASCADE)
    emp_scd1_id = models.IntegerField()

    choices= (
        ('Salary','Salary'),
        ('Bonus','Bonus'),
        ('Advance','Advance'),
        ('Others','Others') #input field
    )

    reason = models.CharField(choices=choices,max_length=50)
    status = models.CharField(max_length=50)
    amount = models.FloatField()
    date = models.DateField()


class Deduction(models.Model):
    row_id = models.IntegerField(primary_key=True)
    emp_id = models.ForeignKey(Employees, on_delete=models.CASCADE) #OneToMany
    emp_scd1_id = models.IntegerField()

    choices = (
        ('Advance','Advance'),
        ('Loan', 'Loan'),
        ('Tax','Tax')
    )

    description = models.CharField(choices=choices,max_length=50)
    amount = models.FloatField()
    current_flag = models.CharField(max_length=50)
    effective_from = models.DateField()
    effective_to = models.DateField()
    created_on = models.DateField()
    updated_on = models.DateField()


class Overtime(models.Model):
    row_id = models.IntegerField(primary_key=True)
    emp_id = models.ForeignKey(Employees, on_delete=models.CASCADE) #oneTOMany
    emp_scd1_id = models.IntegerField()
    working_hour = models.FloatField()
    overtime_scale = models.FloatField()
    overtime_date = models.DateField()


class Admin(models.Model):
    row_id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    current_flag = models.CharField(max_length=50)
    effective_from = models.DateField()
    effective_to = models.DateField()
    created_on = models.DateField()
    updated_on = models.DateField()
