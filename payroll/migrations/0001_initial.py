# Generated by Django 2.1.5 on 2019-02-11 17:17

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('row_id', models.IntegerField(primary_key=True, serialize=False)),
                ('username', models.CharField(max_length=50)),
                ('password', models.CharField(max_length=50)),
                ('firstname', models.CharField(max_length=50)),
                ('lastname', models.CharField(max_length=50)),
                ('current_flag', models.CharField(max_length=50)),
                ('effective_from', models.DateField()),
                ('effective_to', models.DateField()),
                ('created_on', models.DateField()),
                ('updated_on', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Attendance',
            fields=[
                ('row_id', models.IntegerField(primary_key=True, serialize=False)),
                ('date', models.DateField()),
                ('checkin_time', models.TimeField()),
                ('status', models.CharField(max_length=50)),
                ('checkout_time', models.TimeField()),
                ('hours', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Deduction',
            fields=[
                ('row_id', models.IntegerField(primary_key=True, serialize=False)),
                ('emp_scd1_id', models.IntegerField()),
                ('description', models.CharField(choices=[('Advance', 'Advance'), ('Loan', 'Loan'), ('Tax', 'Tax')], max_length=50)),
                ('amount', models.FloatField()),
                ('current_flag', models.CharField(max_length=50)),
                ('effective_from', models.DateField()),
                ('effective_to', models.DateField()),
                ('created_on', models.DateField()),
                ('updated_on', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Employees',
            fields=[
                ('row_id', models.IntegerField(blank=True)),
                ('emp_id', models.IntegerField(primary_key=True, serialize=False, validators=[django.core.validators.MinValueValidator(1)])),
                ('scd1_id', models.IntegerField()),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('address', models.TextField()),
                ('DOB', models.DateField()),
                ('contact_info', models.CharField(max_length=50)),
                ('gender', models.CharField(choices=[('Male', 'Male'), ('Female', 'Female'), ('Unspecified', 'Others')], max_length=50)),
                ('post_id', models.IntegerField()),
                ('post_scd_id', models.IntegerField()),
                ('current_flag', models.CharField(max_length=50)),
                ('effective_from', models.DateField()),
                ('effective_to', models.DateField()),
                ('created_on', models.DateField()),
                ('updated_on', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Overtime',
            fields=[
                ('row_id', models.IntegerField(primary_key=True, serialize=False)),
                ('emp_scd1_id', models.IntegerField()),
                ('working_hour', models.FloatField()),
                ('overtime_scale', models.FloatField()),
                ('overtime_date', models.DateField()),
                ('emp_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='payroll.Employees')),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('row_id', models.IntegerField(primary_key=True, serialize=False)),
                ('emp_scd1_id', models.IntegerField()),
                ('reason', models.CharField(choices=[('Salary', 'Salary'), ('Bonus', 'Bonus'), ('Advance', 'Advance'), ('Others', 'Others')], max_length=50)),
                ('status', models.CharField(max_length=50)),
                ('amount', models.FloatField()),
                ('date', models.DateField()),
                ('emp_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='payroll.Employees')),
            ],
        ),
        migrations.CreateModel(
            name='Position',
            fields=[
                ('row_id', models.IntegerField(primary_key=True, serialize=False)),
                ('scd1_id', models.IntegerField()),
                ('description', models.CharField(max_length=50)),
                ('current_flag', models.CharField(max_length=50)),
                ('effective_from', models.DateField()),
                ('effective_to', models.DateField()),
                ('created_on', models.DateField()),
                ('updated_on', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Salary',
            fields=[
                ('row_id', models.IntegerField(primary_key=True, serialize=False, validators=[django.core.validators.MinValueValidator(1)])),
                ('emp_scd1_id', models.IntegerField()),
                ('basic_scale', models.FloatField()),
                ('other_sal1', models.FloatField()),
                ('other_sal2', models.FloatField()),
                ('other_sal3', models.FloatField()),
                ('net_salary', models.FloatField()),
                ('current_flag', models.CharField(max_length=50)),
                ('effective_from', models.DateField()),
                ('effective_to', models.DateField()),
                ('created_on', models.DateField()),
                ('updated_on', models.DateField()),
                ('emp_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='payroll.Employees')),
            ],
        ),
        migrations.AddField(
            model_name='deduction',
            name='emp_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='payroll.Employees'),
        ),
        migrations.AddField(
            model_name='attendance',
            name='emp_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='payroll.Employees'),
        ),
    ]
