from django.urls import path, include
from .views import *
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.home, name='home'),
    path('home', views.home, name='home'),
    path('display-employee', views.display_employee, name='display-employee'),
    path('add-employee', views.add_employee, name='add-employee'),
    path('display-salary', views.display_salary, name='display-salary'),
    path('add-salary', views.add_salary, name='add-salary'),
    path('edit-salary/<int:emp_id>/', views.edit_salary, name='edit-salary'),
    path('print-slip/<int:emp_id>/', views.print_slip, name='print-slip'),
    path('display-attendance', views.display_attendance, name='display-attendance'),
    path('register/', payroll_views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    path('profile/', views.profile, name='profile'),
    path('view-pdf/', views.view_pdf, name='view-pdf'),

]


# #Add Django site authentication urls (for login, logout, password management)
# urlpatterns += [
#     path('accounts/', include('django.contrib.auth.urls')),
# ]