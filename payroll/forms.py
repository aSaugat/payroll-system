from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import *


class EmployeeForm(forms.ModelForm):
	class Meta:
		model = Employees
		fields = ('row_id','emp_id','scd1_id','first_name','last_name','address','DOB','contact_info','gender',
				  'post_id','post_scd_id','current_flag','effective_from',\
				  'effective_to','created_on','updated_on')

class SalaryForm(forms.ModelForm):
	class Meta:
		model = Salary
		fields = ('row_id','emp_id','emp_scd1_id','basic_scale','other_sal1','other_sal2','other_sal3',
				  'net_salary','current_flag','effective_from',\
				  'effective_to','created_on','updated_on')


class UserRegisterForm(UserCreationForm):
	email = forms.EmailField()

	class Meta:
		model = User
		fields = ('username', 'email', 'password1', 'password2')


