import io

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from . models import *
from .forms import EmployeeForm, SalaryForm, UserRegisterForm
from payroll import views as payroll_views
from django.contrib import messages
import reportlab
from django.http import FileResponse, HttpResponse

from xhtml2pdf import pisa
from io import StringIO, BytesIO
from django.template.loader import get_template
from django.template import Context


# Create your views here.
@login_required
def index(request):
    return render(request, 'index.html')

def home(request):
    return render(request, 'home.html')

@login_required
def display_employee(request):
	tables = Employees.objects.all() #show all the entries present in Laptop model(DB)

	context = {
		'tables' : tables,
		'header' : 'Employees'
	}
	return render(request, 'display_employee.html', context)

@login_required
def add_employee(request):
	if request.method == "POST":
		form = EmployeeForm(request.POST)

		if form.is_valid():
			form.save()
			return redirect('add-employee')


	form = EmployeeForm()
	return render(request, 'add_employee.html', {'form': form})

@login_required
def display_salary(request):
	tables = Salary.objects.all() #show all the entries present in Laptop model(DB)

	context = {
		'tables' : tables,
		'header' : 'Salary'
	}
	return render(request, 'display_salary.html', context)

@login_required
def add_salary(request):
	if request.method == "POST":
		form = SalaryForm(request.POST)

		if form.is_valid():
			form.save()
			return redirect('add-salary')


	form = SalaryForm()
	return render(request, 'add_salary.html', {'form': form})

@login_required
def display_attendance(request):
	tables = Attendance.objects.all() #show all the entries present in Laptop model(DB)

	context = {
		'tables' : tables,
		'header' : 'Attendance'
	}
	return render(request, 'display_attendance.html', context)


def register(request):
	if request.method == "POST":
		form = UserRegisterForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data.get('username')
			messages.success(request, f'You Account has been created. You are now able to login!')
			return redirect('login')
	else:
		form = UserRegisterForm()
	return render(request, 'register.html', {'form': form})


@login_required
def profile(request):
	return render(request, 'users/profile.html')


def edit_salary(request, emp_id):
	item = get_object_or_404(Salary, emp_id = emp_id)

	if request.method == "POST":
		form = SalaryForm(request.POST, instance=item)
		if form.is_valid():
			form.save()
			return redirect('home')

	else:
		form = SalaryForm(instance=item)
		return render(request, 'edit_salary.html', {'form':form})


def print_slip(request, emp_id):
    print(emp_id)
    salary = Salary.objects.get(emp_id=emp_id)  # show the salary table relative to the primary key
    employee = Employees.objects.get(emp_id=emp_id)

    template = get_template("print_slip.html")

    context = {
        'pagesize': 'A4',
        'salary': salary,
        'employee': employee,
        'header': 'Salary'
    }

    html = template.render(context)
    result = BytesIO()
    pdf = pisa.pisaDocument(StringIO(html), dest=result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    else:
        return HttpResponse('Errors')


def view_pdf(request):
    template = get_template("display_salary.html")
    context = {'pagesize':'A4'}
    html = template.render(context)
    result = BytesIO()
    pdf = pisa.pisaDocument(StringIO(html), dest=result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    else:
	    return HttpResponse('Errors')


